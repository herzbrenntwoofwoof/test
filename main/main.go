package main

import (
	"os"
	"io"
	"fmt"
	"strings"
	"go-jinja2"
	"gopkg.in/yaml.v3"
	"path/filepath"
)

func readFile(file string) string {
	fi, err := os.Open(file)
    if err != nil {
        panic(err)
    }

	defer func() {
        if err := fi.Close(); err != nil {
            panic(err)
        }
    }()

	str := new(strings.Builder)
   	io.Copy(str, fi)

	return str.String()
}

func readYaml(file string) map[string]any {
	y := readFile(file)
	var m map[string]any 
	err := yaml.Unmarshal([]byte(y), &m)
	if err != nil {
		panic(err)
	}
	return m
}

func renderTemplate(t string, v map[string]any) string {
	j2, err := jinja2.NewJinja2("sanya", 1, jinja2.WithGlobals(v), jinja2.WithStrict(false), jinja2.WithDebugTrace(true))
	if err != nil {
		panic(err)
	}
	defer j2.Close()
	out, err := j2.RenderString(t)
	if err != nil {
		panic(err)
	}
	return out
}

func main() {

	if len(os.Args) < 2 {
		panic("Надо вызывать в таком виде, друг: jinjer.exe template.yml variable.yml")
	}

	path, err := os.Executable()
	if err != nil {
        panic(err)
    }
	
	exPath := filepath.Dir(path)

	inputFile := readFile(exPath + "\\" + os.Args[1])
	vars := readYaml(exPath + "\\" + os.Args[2])
	output := renderTemplate(inputFile, vars)

	fmt.Printf(output)

	file, err := os.OpenFile("output.yaml", os.O_WRONLY|os.O_CREATE, 0666)
    if err != nil {
        fmt.Println("File does not exists or cannot be created")
        os.Exit(1)
    }
    defer file.Close()

	fmt.Fprintf(file, output)
}