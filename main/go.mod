module jinja_parser/main

go 1.22.1

require (
	go-jinja2 v0.0.0-00010101000000-000000000000
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/go-git/gcfg v1.5.1-0.20230307220236-3a3c6141e376 // indirect
	github.com/go-git/go-billy/v5 v5.5.0 // indirect
	github.com/go-git/go-git/v5 v5.11.0 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/jinzhu/copier v0.4.0 // indirect
	github.com/kluctl/go-embed-python v0.0.0-3.12.2-20240224-1 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	go-jinja2/internal/data v0.0.0 // indirect
	go-jinja2/python_src v0.0.0 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
)

replace go-jinja2 => ..\..\go-jinja2-mod

replace go-jinja2/internal/data => ..\..\go-jinja2-mod\internal\data

replace go-jinja2/python_src => ..\..\go-jinja2-mod\python_src
